FROM golang:1.14-alpine AS build
# ENV GO111MODULE on
ENV CGO_ENABLED 0
# ENV GOOS linux

RUN apk add git make openssl gcc libc-dev

WORKDIR /opt/gpu-admission-webhook
COPY go.sum .
COPY go.mod .
RUN go mod download
ADD . .
RUN go test ./cmd/webhook
RUN go build ./cmd/webhook

FROM alpine
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=build /opt/gpu-admission-webhook/webhook /usr/local/bin/webhook
CMD ["webhook"]
