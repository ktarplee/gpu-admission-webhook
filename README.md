# GPU Admission Webhook

**The problem** is the default behavior in Kubernetes for scheduling GPUs on a shared cluster is far from ideal.  Either not specifying "nvidia.com/gpu" or setting the value to "0" will give the pod access to ALL the GPUs which makes scheduling based on GPUs impossible.  So when a developer asks for resources and they don't even know that there exist GPU resources, they gain access to all GPUs.

**The solution** that this project implements is a MutableAdmissionWebHook for Kubernetes that adds NVIDIA_VISIBLE_DEVICES=none to disable GPU support unless explicitly requested in "resources.limits" by setting "nvidia.com/gpu" to a value greater than "0".  This hoook also removes other NVIDIA and CUDA specific environment variables is specified in the pod spec to ensure that the correct number of GPUs are utilized.  This solution also works with other nvidia.com devices such as "nvidia.com/sharedgpu".  Any resource that starts with nvidia.com/ is captured.

This webhook has been tested in Kubernetes 1.17.x and 1.18.2.

## Installation

`skaffold run` can be used to build the image and install the app from a git repo.

You can also helm install it with `helm install gpu-admission-webhook ./charts/gpu-admission-webhook`

### Building, Testing, and Running

The code can be built, tested, and run with variations of `go run ./cmd/webhook`.  Replace "run" with "build" or "test".

## Testing with KinD

Installation [instructions](https://kind.sigs.k8s.io/docs/user/quick-start) for KinD

```bash
curl -Lo ./kind "https://github.com/kubernetes-sigs/kind/releases/download/v0.8.0/kind-$(uname)-amd64"
chmod +x ./kind
mv ./kind ~/bin/kind
```

Creating a cluster is rather easy (takes a while the first time because it has to build a few images).

```bash
kind create cluster
kubectl label ns kube-system gpu-admission-webhook/disable=true
```

Then we deploy our pod and webhook

```bash
skaffold run
```

Now try mutating the workloads

```bash
kubectl create -f manifests
```

Try a strategic patch a existing deployment (this actually CREATEs a new pod and not UPDATE it)

```bash
kubectl patch deployment test-deployment --patch "$(cat patches/deployment.yaml)"
```

Cleanup

```bash
kubectl delete -f manifests
kind delete cluster
```

## Contributing

Contributors are welcome.

## Links

* [Useful tutorial](https://medium.com/ovni/writing-a-very-basic-kubernetes-mutating-admission-webhook-398dbbcb63ec)
* [Modern Go application](https://github.com/sagikazarmark/modern-go-application)
* [Example Webhook](https://github.com/banzaicloud/admission-webhook-example/blob/master/webhook.go)
* [Validating webhook using a different deserialization technique, CLI, and HTTPS structure](https://github.com/ContainerSolutions/go-validation-admission-controller/)
