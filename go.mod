module gitlab.com/ktarplee/gpu-admission-webhook

go 1.14

require (
	github.com/evanphx/json-patch v4.2.0+incompatible
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20200501053045-e0ff5e5a1de5 // indirect
	k8s.io/api v0.18.2
	k8s.io/apimachinery v0.18.2
	sigs.k8s.io/structured-merge-diff v0.0.0-20190525122527-15d366b2352e // indirect
)
