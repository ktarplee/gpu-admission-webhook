package main

import (
	"encoding/json"
	"testing"
	"io/ioutil"
	"path/filepath"

	"github.com/stretchr/testify/assert"

	admissionV1 "k8s.io/api/admission/v1"
	jsonpatch "github.com/evanphx/json-patch"
)

func TestMutateJSON(t *testing.T) {

	// TODO generate test cases by modifying the manifests

	// test the cases for env
	// (1) env: []
	// (2) env not present
	// and maybe (3) env: [{"name": "NVIDIA_VISIBLE_DEVICES", "value": "all"}]

	// test cases for nvidia.com/gpu
	// (1) resources.limits."nvidia.com/gpu" present
	// (2) resources.limits."nvidia.com/gpu" not present
	// (3) resources.limits."nvidia.com/gpu" set to "0"
	// (4) resources.requests."nvidia.com/gpu" set to "0"

	// Add unsuccessful cases.

	testCases := []struct {
        reqFile  string
        success  bool
        patchSize int
    }{
      	{"pod.json", true, 3},
		{"pod-nogpu.json", true, 2},
		{"pod-init-nogpu.json", true, 4},
		{"pod-env-nogpu.json", true, 4},
		{"pod-gpu0.json", true, 4},
		{"pod-gpu1.json", true, 0},
		{"pod-sharedgpu.json", true, 0},
		{"deployment-pod.json", true, 2},
    }
    for _, tc := range testCases {
		filename := tc.reqFile
		rawJSON, err := ioutil.ReadFile(filepath.Join("testdata", filename))
		assert.NoError(t, err, "failed to read test file %s", filename, err)

        t.Run(tc.reqFile, func(t *testing.T) {
            testOne(t, rawJSON, tc.success, tc.patchSize)
		})
	}
}

func testOne(t *testing.T, rawJSON []byte, success bool, patchSize int) {
	response, err := Mutate(false, rawJSON)
	assert.NoError(t, err, "failed to mutate AdmissionRequest %s with error %s", string(response), err)

	r := admissionV1.AdmissionReview{}
	err = json.Unmarshal(response, &r)
	assert.NoError(t, err, "failed to unmarshal with error %s", err)

	rr := r.Response
	assert.Contains(t, rr.AuditAnnotations, "corrections")

	patch, err := jsonpatch.DecodePatch(rr.Patch)
	assert.NoError(t, err, "failed to parse patch %s", err)
	assert.Equal(t, patchSize, len(patch))

	t.Logf("Original %s\n", string(r.Request.Object.Raw))

	modified, err := patch.Apply(r.Request.Object.Raw)
	assert.NoError(t, err, "failed to apply patch %s", err)

	t.Logf("Patched %s\n", string(modified))
}
