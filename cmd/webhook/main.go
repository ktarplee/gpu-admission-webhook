package main

import (
	"flag"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func handleHealth(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello %q", html.EscapeString(r.URL.Path))
}

func handleMutate(verbose bool, w http.ResponseWriter, r *http.Request) {

	// read the body / request
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%s", err)
		return
	}

	// mutate the request
	mutated, err := Mutate(verbose, body)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%s", err)
		return
	}

	// and write it back
	w.WriteHeader(http.StatusOK)
	w.Write(mutated)
}

func main() {

	var httpPort, httpsPort int
	var certFile string
	var keyFile string
	var verbose bool

	// get command line parameters
	flag.IntVar(&httpPort, "httpPort", 8080, "Webhook HTTP server port.")
	flag.IntVar(&httpsPort, "httpsPort", 8443, "Webhook HTTPS server port.")
	flag.StringVar(&certFile, "cert", "tls.crt", "File containing the x509 certificate for HTTPS.")
	flag.StringVar(&keyFile, "key", "tls.key", "File containing the x509 private key for HTTPS.")
	flag.BoolVar(&verbose, "v", false, "Verbose logging")
	flag.Parse()

	mux := http.NewServeMux()

	mux.HandleFunc("/health", handleHealth)
	mux.HandleFunc("/mutate", func(w http.ResponseWriter, r *http.Request) {
		handleMutate(verbose, w, r);
	})

	httpServer := &http.Server{
		Addr:           fmt.Sprintf(":%d", httpPort),
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20, // 1048576
	}

	httpsServer := &http.Server{
		Addr:           fmt.Sprintf(":%d", httpsPort),
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20, // 1048576
	}

	fmt.Printf("Listening on http://localhost:%d and https://localhost:%d\n", httpPort, httpsPort)

	go func() {
		log.Fatal(httpServer.ListenAndServe())
	}()

	log.Fatal(httpsServer.ListenAndServeTLS(certFile, keyFile))

}
