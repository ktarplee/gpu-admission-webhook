// Deals with AdmissionReview requests and responses, it takes in the request body and returns a readily converted JSON []byte that can be
// returned from a http Handler w/o needing to further convert or modify it, it also makes testing Mutate() kind of easy w/o need for a fake http server, etc.
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	admissionV1 "k8s.io/api/admission/v1" // there is also a v1 but k8s 1.16 seems to require v1beta1 to be supported at a minimum
	coreV1 "k8s.io/api/core/v1"
	// appsV1 "k8s.io/api/apps/v1"
	// batchV1 "k8s.io/api/batch/v1"
	// batchV1beta1 "k8s.io/api/batch/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Relevant docs on AdmissionReview is here
// https://github.com/kubernetes/api/blob/master/admission/v1/types.go#L40

// JSON Patch format http://jsonpatch.com
type patchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

// Mutate mutates
func Mutate(verbose bool, body []byte) ([]byte, error) {
	if verbose {
		log.Printf("Received: %s", string(body))
	}

	// unmarshal request into AdmissionReview struct
	admReview := admissionV1.AdmissionReview{}
	if err := json.Unmarshal(body, &admReview); err != nil {
		return nil, fmt.Errorf("unable to unmarshal request failed with %s", err)
	}
	responseBody := []byte{}
	req := admReview.Request

	if req == nil {
		return responseBody, nil
	}

	kind := req.Kind.Kind
	log.Printf("Processing %s\n", kind)

	var patches []patchOperation
	switch kind {
	case "Pod":
		var pod *coreV1.Pod
		// get the Pod object and unmarshal it into its struct, if we cannot, we might as well stop here
		if err := json.Unmarshal(req.Object.Raw, &pod); err != nil {
			return nil, fmt.Errorf("unable to unmarshal Pod %v", err)
		}
		// the actual mutation is done by a string in JSONPatch style, i.e. we don't _actually_ modify the object, but
		// tell K8S how it should modify it
		p := mutatePodSpec("/spec", pod.Spec)
		patches = append(patches, p...)
	/*
	case "Deployment":
		var app *appsV1.Deployment
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal Deployment %v", err)
		}
		p := mutatePodSpec("/spec/template/spec", app.Spec.Template.Spec)
		patches = append(patches, p...)

	case "StatefulSet":
		var app *appsV1.StatefulSet
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal StatefulSet %v", err)
		}
		p := mutatePodSpec("/spec/template/spec", app.Spec.Template.Spec)
		patches = append(patches, p...)

	case "ReplicaSet":
		var app *appsV1.ReplicaSet
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal ReplicaSet %v", err)
		}
		p := mutatePodSpec("/spec/template/spec", app.Spec.Template.Spec)
		patches = append(patches, p...)

	case "DaemonSet":
		var app *appsV1.DaemonSet
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal DaemonSet %v", err)
		}
		p := mutatePodSpec("/spec/template/spec", app.Spec.Template.Spec)
		patches = append(patches, p...)

	case "Job":
		var app *batchV1.Job
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal Job %v", err)
		}
		p := mutatePodSpec("/spec/template/spec", app.Spec.Template.Spec)
		patches = append(patches, p...)

	case "CronJob":
		var app *batchV1beta1.CronJob
		if err := json.Unmarshal(req.Object.Raw, &app); err != nil {
			return nil, fmt.Errorf("unable to unmarshal CronJob %v", err)
		}
		p := mutatePodSpec("/spec/jobTemplate/spec/template/spec", app.Spec.JobTemplate.Spec.Template.Spec)
		patches = append(patches, p...)
	*/
	default:
		return nil, fmt.Errorf("Unrecognized kind %v", kind)
	}

	var err error
	resp := admissionV1.AdmissionResponse{}

	// parse the []map into JSON
	resp.Patch, err = json.Marshal(patches)
	if err != nil {
		return nil, err
	}
	log.Printf("Patches: %s\n", resp.Patch)

	// set response options
	resp.Allowed = true
	resp.UID = req.UID
	pT := admissionV1.PatchTypeJSONPatch
	resp.PatchType = &pT // it's annoying that this needs to be a pointer as you cannot give a pointer to a constant?

	// add some audit annotations, helpful to know why a object was modified
	resp.AuditAnnotations = map[string]string{
		"corrections": string(len(patches)),
	}

	// Success, of course ;)
	resp.Result = &metaV1.Status{
		Status: "Success",
	}

	admReview.Response = &resp
	// back into JSON so we can return the finished AdmissionReview w/ Response directly
	// w/o needing to convert things in the http handler
	responseBody, err = json.Marshal(admReview)
	if err != nil {
		return nil, err
	}

	// log.Printf("Response: %s\n", string(responseBody))
	return responseBody, nil
}

func mutatePodSpec(path string, podSpec coreV1.PodSpec) (patches []patchOperation){
	// we handle initContainers as well t be safe
	for i := range podSpec.InitContainers {
		p := mutateContainer(fmt.Sprintf("%s/initContainers/%d", path, i), podSpec.InitContainers[i])
		patches = append(patches, p...)
	}

	for i := range podSpec.Containers {
		p := mutateContainer(fmt.Sprintf("%s/containers/%d", path, i), podSpec.Containers[i])
		patches = append(patches, p...)
	}
	return
}

func mutateContainer(path string, con coreV1.Container) (p []patchOperation){
	
	// start by removing all environment variables that do not belong.
	// log.Printf("Envs are: %+v\n", con.Env)

	disallowedEnv := map[string]bool{
		"NVIDIA_VISIBLE_DEVICES": true,
		"NVIDIA_DRIVER_CAPABILITIES": true,
		"CUDA_VISIBLE_DEVICES": true,
	}

	// loop over the list backwords to make sure the JSON Patches are applied in the correct order
	for i := len(con.Env)-1; i >= 0; i-- {
		env := con.Env[i]
		_, disallowed := disallowedEnv[env.Name]
		if strings.HasPrefix(env.Name, "NVIDIA_REQUIRE_") {
			disallowed = true
		}
		if disallowed {
			// add a patch to remove it
			p = append(p, patchOperation{
				Op:    "remove",
				Path:  fmt.Sprintf("%s/env/%d", path, i),
			})
		}
	}
	
	log.Printf("Resources are: %+v\n", con.Resources)
	needsGPU := false
	for name, value := range con.Resources.Limits {
		if strings.HasPrefix(name.String(), "nvidia.com/") {
			if value.IsZero() {
				// not actually requesting a GPU
				// remove the entry because they don't want a GPU and we don't want the device pluggin to get it's hands on this Container
				variant := strings.TrimPrefix(name.String(), "nvidia.com/")
				p = append(p, patchOperation{
					Op:    "remove",
					Path:  path+"/resources/limits/nvidia.com~1"+variant,
				})

				// delete the requests as well (if present)
				if _, present := con.Resources.Requests[coreV1.ResourceName("nvidia.com/"+variant)]; present {
					p = append(p, patchOperation{
						Op:    "remove",
						Path:  path+"/resources/requests/nvidia.com~1"+variant,
					})
				}
			} else {
				needsGPU = true
			}
		}
	}

	if !needsGPU {
		// no GPU requested, let's ensure the GPU is not accessible

		// ensure env is present (otherwise we cannot append to it down below)
		if len(con.Env) == 0 {
			p = append(p, patchOperation{
				Op:    "add",
				Path:  path+"/env",
				Value: []coreV1.EnvVar{},
			})
		}

		// We use "none" to allow the libraries to still exist (for runtime purposes) but no GPU
		// "void" is all possible
		// see https://github.com/NVIDIA/nvidia-container-runtime
		p = append(p, patchOperation{
			Op:    "add",
			Path:  path+"/env/-", // add it to the end of env
			Value: coreV1.EnvVar{Name: "NVIDIA_VISIBLE_DEVICES", Value: "none", ValueFrom: nil},
		})
	}

	return
}
