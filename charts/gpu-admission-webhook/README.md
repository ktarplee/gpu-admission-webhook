# GPU Admission Webhook

Implementation of a MutableAdmissionWebHook for k8s that adds NVIDIA_VISIBLE_DEVICES=none to disable GPU support unless explicitly requested in resources.limits by setting nvidia.com/gpu to a value greater than "0".  The default behavior of either not specifying nvidia.com/gpu or setting the value to "0" will give the pod access to ALL the GPUs which makes scheduling based on GPUs impossible.  This also works with any "nvidia.com/*" device such as "nvidia.com/sharedgpu".

## Configuration

There are some setting you will likely need to configure.

The following table lists common configurable parameters of the chart and
their default values. See values.yaml for all available options.

| Parameter            | Description                                                                                  | Default                     |
|----------------------|----------------------------------------------------------------------------------------------|-----------------------------|
| `debug`              | Increase the verbosity of the logs.                                                          | `no`                        |
| `label`              | Label base to use for determining which pods should be ignored when processing.              | `gpu-admission-webhook`     |
| `nsMatchExpressions` | Contains standard matchExpressions for namespaces that should not be annotated.              | `[]`                        |

It is probably best to not modify anything installed in kube-system.  To do this label the kube-system namespace so that it does not get modified by the webhook.

```bash
kubectl label ns kube-system gpu-admission-webhook.act3-ace.io/disable=true
```

You may also exclude namespaces with arbitrary matchExpressions such as:

```yaml
nsMatchExpressions:
  - key: field.cattle.io/projectId # <-- you might need to change this
    operator: NotIn
    values:
      - foo # <-- you need to change this
```

## Installation

`helm install gpu-admission-webhook ./charts/gpu-admission-webhook`

You can then deploy a pod and look at it with `kubectl describe pod` to see if NVIDIA_VISIBLE_DEVICES was set.
